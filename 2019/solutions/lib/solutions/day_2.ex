defmodule Solutions.Day2 do
  @opcode_add 1
  @opcode_multiply 2

  def execute_intcode(ints, pos_ptr \\ 0) do
    # IO.puts(inspect(ints))
    # IO.puts(inspect(Enum.slice(ints, pos_ptr, 4)))

    # take 4, [operator, first value, second value, save register]
    case Enum.slice(ints, pos_ptr, 4) do
      [@opcode_add, first, second, register] ->
        result = Enum.fetch!(ints, first) + Enum.fetch!(ints, second)
        execute_intcode(List.replace_at(ints, register, result), pos_ptr + 4)

      [@opcode_multiply, first, second, register] ->
        result = Enum.fetch!(ints, first) * Enum.fetch!(ints, second)
        execute_intcode(List.replace_at(ints, register, result), pos_ptr + 4)

      [99 | _] ->
        ints
    end
  end

  def patch_intcode(ints, pos_one, pos_two) do
    ints
    |> List.replace_at(1, pos_one)
    |> List.replace_at(2, pos_two)
  end

  def result(intcode, pos_one, pos_two) do
    [result | _] =
      intcode
      |> patch_intcode(pos_one, pos_two)
      |> execute_intcode

    result
  end

  def part_1(intcode) do
    result(intcode, 12, 2)
  end

  def part_2(intcode) do
    try do
      for noun <- 0..99, verb <- 0..99 do
        res = result(intcode, noun, verb)

        if res == 19_690_720 do
          throw({:break, {noun, verb}})
        end
      end
    catch
      {:break, {noun, verb}} -> 100 * noun + verb
    end
  end

  def answer(input) do
    list = String.split(input, ",")

    int_list =
      Enum.map(
        list,
        fn el -> String.to_integer(el) end
      )

    IO.puts(part_1(int_list))
    IO.puts(part_2(int_list))
  end

  def answer do
    contents = File.read!("priv/day_2_input.txt")
    answer(String.trim(contents))
  end
end
