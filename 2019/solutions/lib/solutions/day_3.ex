defmodule Solutions.Day3 do
  # if we run out of instructions return
  def draw_path(map, [], ident, _, _) do
    map
  end

  def draw_path(map, path, ident, x \\ 0, y \\ 0) do
    [instr | remainder] = path
    # IO.puts "=============="
    # IO.puts("X: #{inspect x} Y: #{inspect y}")
    # IO.puts(inspect map)
    # IO.puts "Instruction: #{inspect instr}"
    # IO.puts "Tail: #{inspect remainder}"
    {result, nextInstr, nextPos} =
      case instr do
        # out of executions at this level
        {_, -0} ->
          {:next, {}, {}}

        {:right, dist} ->
          {:ok, {:right, dist - 1}, {x + 1, y}}

        {:left, dist} ->
          {:ok, {:left, dist - 1}, {x - 1, y}}

        {:up, dist} ->
          {:ok, {:up, dist - 1}, {x, y + 1}}

        {:down, dist} ->
          {:ok, {:down, dist - 1}, {x, y - 1}}
      end

    # check that we haven't exhausted a direction
    if result == :next do
      draw_path(map, remainder, ident, x, y)
    else
      #  IO.puts "Next position #{inspect nextPos}"
      #  IO.puts "Fetch: #{inspect Map.fetch(map, nextPos)}"
      case Map.fetch(map, nextPos) do
        {:ok, "o"} ->
          # don't overwrite the start location
          draw_path(map, [nextInstr | remainder], ident, elem(nextPos, 0), elem(nextPos, 1))

        {:ok, ^ident} ->
          # passing over self, no worries.
          draw_path(
            Map.put(map, nextPos, ident),
            [nextInstr | remainder],
            ident,
            elem(nextPos, 0),
            elem(nextPos, 1)
          )

        {:ok, prevIdent} ->
          # different identity was here
          draw_path(
            Map.put(map, nextPos, "X"),
            [nextInstr | remainder],
            ident,
            elem(nextPos, 0),
            elem(nextPos, 1)
          )

        _ ->
          draw_path(
            Map.put(map, nextPos, ident),
            [nextInstr | remainder],
            ident,
            elem(nextPos, 0),
            elem(nextPos, 1)
          )
      end
    end
  end

  def part_1(path_1, path_2) do
    map = %{{0, 0} => "o"}
    map = draw_path(map, path_1, 1)
    IO.puts(inspect(map))
    map = draw_path(map, path_2, 2)

    min_dist =
      Enum.reduce(map, -1, fn el, min_dist ->
        {{x, y}, value} = el
        IO.puts("location: #{inspect(x)} #{inspect(y)} #{inspect(value)}")
        manhatten_dist = Kernel.abs(x) + Kernel.abs(y)
        IO.puts("Manhatten distance #{inspect(manhatten_dist)}")
        IO.puts("Current min distance #{inspect(min_dist)}")
        # if wires are crossed
        if value == "X" do
          if min_dist == -1 do
            manhatten_dist
          else
            min(manhatten_dist, min_dist)
          end
        else
          min_dist
        end
      end)

    IO.puts(inspect(min_dist))

    min_dist
  end

  def part_2(_path_1, _path_2) do
  end

  @doc """
  Parse a string and returns direction and distance

  ## Examples
      iex> Solutions.Day3.parse_instruction("R4")
      {:right, 4}
      iex> Solutions.Day3.parse_instruction("L14")
      {:left, 14}

  """
  def parse_instruction(string) do
    {dir_str, dist_str} = String.split_at(string, 1)

    direction =
      case dir_str do
        "R" -> :right
        "L" -> :left
        "U" -> :up
        "D" -> :down
      end

    distance = String.to_integer(dist_str)
    {direction, distance}
  end

  def load_path(path_string) do
    Enum.map(
      path_string
      |> String.split(","),
      fn instr -> parse_instruction(instr) end
    )
  end

  def load do
    [path_1_string, path_2_string] =
      File.stream!("priv/day_3_input.txt")
      |> Stream.map(&String.trim_trailing/1)
      |> Enum.to_list()

    path_1 = load_path(path_1_string)
    path_2 = load_path(path_2_string)

    [path_1, path_2]
  end

  def answer(path_1, path_2) do
    IO.puts(inspect(path_1))
    IO.puts(inspect(path_2))
    IO.puts(part_1(path_1, path_2))
    # IO.puts(part_2(path_2))
  end

  def answer do
    [path_1, path_2] = load()

    answer(path_1, path_2)
  end
end
