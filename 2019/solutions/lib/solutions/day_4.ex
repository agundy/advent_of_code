defmodule Solutions.Day4 do
  def has_two_matching_adjacent_digits?(number) do
    {digit, count} =
      Integer.digits(number)
      |> Enum.reduce_while({-1, 0}, fn x, acc ->
        {prev_value, count} = acc
        case {prev_value, count} do
          {^x, count} ->
            {:cont, {x, count + 1}}
          {_, 1} ->
            {:halt, {x, true}}
          _ ->
            {:cont, {x, 0}}
        end
      end)

    if count == true or count == 1 do
      true
    else
      false
    end
  end

  def never_decreases?(number) do
    result =
      Integer.digits(number)
      |> Enum.reduce_while(-1, fn x, acc ->
        if x >= acc, do: {:cont, x}, else: {:halt, false}
      end)

    if result != false do
      true
    else
      false
    end
  end

  def answer do
    start_val = 124_075
    end_val = 580_769

    start_val..end_val
    |> Stream.filter(&has_two_matching_adjacent_digits?/1)
    |> Stream.filter(&never_decreases?/1)
    |> Enum.count
  end
end
