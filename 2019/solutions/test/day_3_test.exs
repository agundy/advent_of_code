defmodule Solutions.Day3Test do
  use ExUnit.Case
  doctest Solutions.Day3
  alias Solutions.Day3

  @example_0_path_1 "R8,U5,L5,D3"
  @example_0_path_2 "U7,R6,D4,L4"

  @example_1_path_1 "R75,D30,R83,U83,L12,D49,R71,U7,L72"
  @example_1_path_2 "U62,R66,U55,R34,D71,R55,D58,R83"

  @example_2_path_1 "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
  @example_2_path_2 "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"

  test "check example 0" do
    path_1 = Day3.load_path(@example_0_path_1)
    path_2 = Day3.load_path(@example_0_path_2)
    assert Day3.part_1(path_1, path_2) == 6
  end

  test "check example 1" do
    path_1 = Day3.load_path(@example_1_path_1)
    path_2 = Day3.load_path(@example_1_path_2)
    assert Day3.part_1(path_1, path_2) == 159
  end

  test "check example 2" do
    path_1 = Day3.load_path(@example_2_path_1)
    path_2 = Day3.load_path(@example_2_path_2)
    assert Day3.part_1(path_1, path_2) == 135
  end
end
