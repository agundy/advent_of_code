defmodule Solutions.Day4Test do
  use ExUnit.Case
  doctest Solutions.Day4
  alias Solutions.Day4

  test "has_two_matching_adjacent_digits? works" do
    assert Day4.has_two_matching_adjacent_digits?(11111) == false
    assert Day4.has_two_matching_adjacent_digits?(10101) == false
    assert Day4.has_two_matching_adjacent_digits?(123444) == false
    assert Day4.has_two_matching_adjacent_digits?(111122) == true
    assert Day4.has_two_matching_adjacent_digits?(112233) == true
    assert Day4.has_two_matching_adjacent_digits?(1234445) == false
  end

  test "never_decreases? works" do
    assert Day4.never_decreases?(11112) == true
    assert Day4.never_decreases?(12345) == true
    assert Day4.never_decreases?(10101) == false
  end

end
