defmodule Solutions.Day1 do
  alias Solutions.Helpers

  def part_1(expenses) do
    expenses = Enum.map(expenses, &String.to_integer/1)

    sums_to_2020? = fn i, j -> i + j == 2020 end

    answers = for i <- expenses, j <- expenses, sums_to_2020?.(i, j), do: {i, j}
    {i, j} = List.first(answers)
    i * j
  end

  def part_2(expenses) do
    expenses = Enum.map(expenses, &String.to_integer/1)

    sums_to_2020? = fn i, j, k -> i + j + k == 2020 end

    answers =
      for i <- expenses, j <- expenses, k <- expenses, sums_to_2020?.(i, j, k), do: {i, j, k}

    {i, j, k} = List.first(answers)
    i * j * k
  end

  def answer do
    expenses = Helpers.load_list_file("priv/day_1_input.txt")
    part_1 = part_1(expenses)
    part_2 = part_2(expenses)

    IO.inspect(part_1)
    IO.inspect(part_2)
  end
end
