defmodule Solutions.Day2 do
  alias Solutions.Helpers

  def part_1(lines) do
    lines
    |> Enum.filter(fn line ->
      IO.inspect(line)
      password_valid_part_1?(line)
    end)
    |> Enum.count()
  end

  def part_2(lines) do
    lines
    |> Enum.filter(fn line ->
      IO.inspect(line)
      password_valid_part_2?(line)
    end)
    |> Enum.count()
  end

  def answer do
    expenses = Helpers.load_list_file("priv/day_2_input.txt")
    part_1 = part_1(expenses)
    part_2 = part_2(expenses)

    IO.inspect(part_1)
    IO.inspect(part_2)
  end

  defp password_valid_part_1?(line) do
    [range_str, letter_str, password] = String.split(line, " ")
    {min, max} = parse_min_max(range_str)
    letter = String.first(letter_str)

    {_chars, count} =
      Enum.flat_map_reduce(String.codepoints(password), 0, fn val, acc ->
        cond do
          acc == max and val == letter ->
            {:halt, acc + 1}

          val == letter ->
            {[val], acc + 1}

          true ->
            {[val], acc}
        end
      end)

    valid = min <= count and count <= max
    valid
  end

  defp password_valid_part_2?(line) do
    [index_str, letter_str, password] = String.split(line, " ")
    {first, second} = parse_min_max(index_str)
    letter = String.first(letter_str)
    first_letter = String.at(password, first - 1)
    second_letter = String.at(password, second - 1)

    valid =
      cond do
        first_letter == letter and second_letter == letter ->
          false

        first_letter == letter ->
          true

        second_letter == letter ->
          true

        true ->
          false
      end

    valid
  end

  defp parse_min_max(range_str) do
    [min, max] = String.split(range_str, "-")
    {String.to_integer(min), String.to_integer(max)}
  end
end
