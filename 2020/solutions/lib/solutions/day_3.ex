defmodule Solutions.Day3 do
  alias Solutions.Helpers

  def part_1(landscape) do
    x = 0
    y = 0

    goal = length(landscape)
    check_tree(landscape, goal, x, y, 3, 1)
  end

  def part_2(landscape) do
    goal = length(landscape)

    [
      {1, 1},
      {3, 1},
      {5, 1},
      {7, 1},
      {1, 2}
    ]
    |> Enum.reduce(1, fn {x_off, y_off}, acc ->
      check_tree(landscape, goal, 0, 0, x_off, y_off) * acc
    end)
  end

  def answer do
    input = Helpers.load_list_file("priv/day_3_input.txt")
    part_1 = part_1(input)
    part_2 = part_2(input)

    IO.inspect(part_1)
    IO.inspect(part_2)
  end

  defp check_tree(_landscape, goal, _x, y, _offset_x, _offset_y) when y >= goal, do: 0

  defp check_tree(landscape, goal, x, y, offset_x, offset_y) do
    row = Enum.at(landscape, y)
    value = String.at(row, rem(x, String.length(row)))

    if value == "#" do
      check_tree(landscape, goal, x + offset_x, y + offset_y, offset_x, offset_y) + 1
    else
      check_tree(landscape, goal, x + offset_x, y + offset_y, offset_x, offset_y)
    end
  end
end
