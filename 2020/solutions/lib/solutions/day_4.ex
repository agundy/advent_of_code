defmodule Passport do
  @moduledoc """
  Validation rules

    byr (Birth Year) - four digits; at least 1920 and at most 2002.
    iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    hgt (Height) - a number followed by either cm or in:
        If cm, the number must be at least 150 and at most 193.
        If in, the number must be at least 59 and at most 76.
    hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    pid (Passport ID) - a nine-digit number, including leading zeroes.
    cid (Country ID) - ignored, missing or not.

  """
  defstruct byr: nil, iyr: nil, eyr: nil, hgt: nil, hcl: nil, ecl: nil, pid: nil, cid: nil

  def santas_valid?(%Passport{} = passport) do
    passport |> Map.drop([:cid]) |> Map.values() |> Enum.all?
  end

  def stricter_santas_valid?(%Passport{} = passport) do
    passport |> Map.from_struct |> Enum.map(
      fn {key, val} -> 
        valid?(key, val)
      end
    ) |> Enum.all?
  end

  def valid?(:byr, nil), do: false
  def valid?(:byr, value) do
    number = String.to_integer(value)
    number >= 1920 and number <= 2002
  end

  def valid?(:iyr, nil), do: false
  def valid?(:iyr, value) do
    number = String.to_integer(value)
    number >= 2010 and number <= 2020
  end

  def valid?(:eyr, nil), do: false
  def valid?(:eyr, value) do
    number = String.to_integer(value)
    number >= 2020 and number <= 2030
  end

  def valid?(:hgt, nil), do: false
  def valid?(:hgt, value) do

    {value, units} = String.split_at(value, -2)
    number = case Integer.parse(value) do
      {number, _} -> number
      _ -> -1
    end

    case {units, number} do
      {"in", inches } when inches in 59..76  ->
        true
      {"cm", cm } when cm in 150..193 ->
        true
      _ -> false
    end
  end

  def valid?(:hcl, nil), do: false
  def valid?(:hcl, value) do
    value =~ ~r/\#[0-9a-f]{6}/
  end

  def valid?(:ecl, value) do
   Enum.member?([
      "amb",
      "blu",
      "brn",
      "gry",
      "grn",
      "hzl",
      "oth"
    ], value)
  end

  def valid?(:pid, nil), do: false
  def valid?(:pid, value) do
    value =~ ~r/^[0-9]{9}$/
  end

  def valid?(:cid, _value) do
    true
  end
end

defmodule Solutions.Day4 do
  def part_1(file) do
    passports = load_passports(file)
    passports_valid = Enum.map(passports, fn passport -> Passport.santas_valid?(passport) end)

    Enum.filter(passports_valid, &(&1)) |> Enum.count()
  end

  def part_2(file) do
    passports = load_passports(file)
    passports_valid = Enum.map(passports, fn passport -> Passport.stricter_santas_valid?(passport) end)

    Enum.filter(passports_valid, &(&1)) |> Enum.count()
  end

  def answer do
    input = File.read!("priv/day_4_input.txt")
  
    part_1 = part_1(input)
    part_2 = part_2(input)

    IO.inspect(part_1)
    IO.inspect(part_2)
  end

  defp load_passports(file) do
    raw_passports = String.split(file, "\n\n")

    passports =
      raw_passports
      |> Enum.map(fn lines -> String.split(lines) end)

    Enum.map(passports, fn passport_parts->
      parse_passport(passport_parts, %Passport{})
    end)
  end

  defp parse_passport([], acc), do: acc

  defp parse_passport([field | rem], %Passport{} = passport) do
    acc =
      case field do
        "byr:" <> value ->
          %{passport | byr: value}

        "iyr:" <> value ->
          %{passport | iyr: value}

        "eyr:" <> value ->
          %{passport | eyr: value}

        "hgt:" <> value ->
          %{passport | hgt: value}

        "hcl:" <> value ->
          %{passport | hcl: value}

        "ecl:" <> value ->
          %{passport | ecl: value}

        "pid:" <> value ->
          %{passport | pid: value}

        "cid:" <> value ->
          %{passport | cid: value}
      end

    parse_passport(rem, acc)
  end
end
