defmodule Solutions.Day5 do
  alias Solutions.Helpers

  def calculate_seat_id("", {row, row}, {seat, seat}) do
    row * 8 + seat
  end

  def calculate_seat_id("F" <> rem, {min_row, max_row} = row, seat) do
    max_row = max_row - div(max_row - min_row, 2) - 1
    calculate_seat_id(rem, {min_row, max_row}, seat)
  end

  def calculate_seat_id("B" <> rem, {min_row, max_row} = row, seat) do
    min_row = min_row + div(max_row - min_row, 2) + 1
    calculate_seat_id(rem, {min_row, max_row}, seat)
  end

  def calculate_seat_id("L" <> rem, row, {min_seat, max_seat}=seat) do
    max_seat = max_seat - div(max_seat - min_seat, 2) - 1
    calculate_seat_id(rem, row, {min_seat, max_seat})
  end

  def calculate_seat_id("R" <> rem, row, {min_seat, max_seat}=seat) do
    min_seat = min_seat + div(max_seat - min_seat, 2) + 1
    calculate_seat_id(rem, row, {min_seat, max_seat})
  end

  def calculate_seat_id(bp, row \\ {0, 127}, seat \\ {0, 7}) do
    calculate_seat_id(bp, row, seat)
  end

  def search_passes(_, []), do: nil

  def search_passes(prev, [bp_id | rem]) do

    if prev + 1 != bp_id do
      bp_id - 1
    else
      search_passes(bp_id, rem)
    end
  end

  def part_1(passes) do
    Enum.map(passes, &(calculate_seat_id(&1))) |> Enum.max
  end

  def part_2(passes) do
    bp_ids = Enum.map(passes, &(calculate_seat_id(&1)))
    |> Enum.sort()
    search_passes(hd(bp_ids) - 1, bp_ids)
  end

  def answer do
    input = Helpers.load_list_file("priv/day_5_input.txt")
    part_1 = part_1(input)
    part_2 = part_2(input)

    IO.inspect(part_1)
    IO.inspect(part_2)
  end
end
