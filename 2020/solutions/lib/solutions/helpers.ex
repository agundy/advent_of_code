defmodule Solutions.Helpers do
  def load_list_file(file_path) do
    File.stream!(file_path)
    |> Stream.map(&String.trim_trailing/1)
    |> Enum.to_list()
  end
end
