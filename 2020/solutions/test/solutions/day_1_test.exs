defmodule Solutions.Day1Test do
  use ExUnit.Case
  alias Solutions.Day1

  @example_expenses [
    "1721",
    "979",
    "366",
    "299",
    "675",
    "1456"
  ]

  test "works for part 1" do
    assert Day1.part_1(@example_expenses) == 514_579
  end

  test "works for part 2" do
    assert Day1.part_2(@example_expenses) == 241_861_950
  end
end
