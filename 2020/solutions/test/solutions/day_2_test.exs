defmodule Solutions.Day2Test do
  use ExUnit.Case
  alias Solutions.Day2

  @example_input [
    "1-3 a: abcde",
    "1-3 b: cdefg",
    "1-9 c: ccccccccc"
  ]

  test "works for part 1" do
    assert Day2.part_1(@example_input) == 2
  end

  test "works for part 2" do
    assert Day2.part_2(@example_input) == 1
  end
end
