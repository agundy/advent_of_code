defmodule Solutions.Day3Test do
  use ExUnit.Case
  alias Solutions.Day3

  @example_input [
    "..##.......",
    "#...#...#..",
    ".#....#..#.",
    "..#.#...#.#",
    ".#...##..#.",
    "..#.##.....",
    ".#.#.#....#",
    ".#........#",
    "#.##...#...",
    "#...##....#",
    ".#..#...#.#"
  ]

  test "works for part 1" do
    assert Day3.part_1(@example_input) == 7
  end

  test "works for part 2" do
    assert Day3.part_2(@example_input) == 336
  end
end
