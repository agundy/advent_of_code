defmodule Solutions.Day4Test do
  use ExUnit.Case
  alias Solutions.Day4

  @example_input """
  ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
  byr:1937 iyr:2017 cid:147 hgt:183cm

  iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
  hcl:#cfa07d byr:1929

  hcl:#ae17e1 iyr:2013
  eyr:2024
  ecl:brn pid:760753108 byr:1931
  hgt:179cm

  hcl:#cfa07d eyr:2025 pid:166559648
  iyr:2011 ecl:brn hgt:59in
  """

  test "works for part 1" do
    assert Day4.part_1(@example_input) == 2
  end

  test "works for part 2" do
    assert Day4.part_2(@example_input) == 2
  end

  describe "validations" do
    """
    byr valid:   2002
    byr invalid: 2003

    hgt valid:   60in
    hgt valid:   190cm
    hgt invalid: 190in
    hgt invalid: 190

    hcl valid:   #123abc
    hcl invalid: #123abz
    hcl invalid: 123abc

    ecl valid:   brn
    ecl invalid: wat

    pid valid:   000000001
    pid invalid: 0123456789
    """

    test "byr valididation" do
      assert Passport.valid?(:byr, "2002")
      refute Passport.valid?(:byr, "2003")
    end

    test "hgt valididation" do
      assert Passport.valid?(:hgt, "60in")
      assert Passport.valid?(:hgt, "190cm")
      refute Passport.valid?(:hgt, "190in")
      refute Passport.valid?(:hgt, "190")
    end

    test "hcl valididation" do
      assert Passport.valid?(:hcl, "#123abc")
      refute Passport.valid?(:hcl, "#123abz")
      refute Passport.valid?(:hcl, "123abc")
    end

    test "ecl valididation" do
      assert Passport.valid?(:ecl, "brn")
      refute Passport.valid?(:ecl, "wat")
    end

    test "pid valididation" do
      assert Passport.valid?(:pid, "000000001")
      refute Passport.valid?(:pid, "0123456789")
    end
  end
end
