defmodule Solutions.Day5Test do
  use ExUnit.Case
  alias Solutions.Day5
  @example_input [
    "FBFBBFFRLR",
    "BFFFBBFRRR",
    "FFFBBBFRRR",
    "BBFFBBFRLL"
  ]

  test "boarding pass ids" do
    assert Day5.calculate_seat_id("FBFBBFFRLR") == 357
    assert Day5.calculate_seat_id("BFFFBBFRRR") == 567
    assert Day5.calculate_seat_id("FFFBBBFRRR") == 119
    assert Day5.calculate_seat_id("BBFFBBFRLL") == 820
  end

  test "works for part 1" do
    assert Day5.part_1(@example_input) == 820
  end
end
