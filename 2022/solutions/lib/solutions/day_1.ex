defmodule Solutions.Day1 do
  alias Solutions.Helpers

  def part_1(elves) do
    elves
    |> Enum.max()
  end

  def part_2(elves) do
    elves
    |> Enum.sort()
    |> Enum.reverse()
    |> Enum.take(3)
    |> Enum.sum()
  end

  def answer do
    input = Helpers.load_list_file("priv/day_1_input.txt")

    elves =
      Enum.chunk_by(input, fn el -> el == "" end)
      |> Enum.reject(&(&1 == [""]))
      |> Enum.map(fn snacks ->
        Enum.map(snacks, &String.to_integer/1) |> Enum.sum()
      end)

    part_1 = part_1(elves)
    part_2 = part_2(elves)

    IO.inspect(part_1)
    IO.inspect(part_2)
  end
end
