defmodule Solutions.Day2 do
  alias Solutions.Helpers

  @rock "A"
  @paper "B"
  @scissors "C"

  @points %{
    @rock => 1,
    @paper => 2,
    @scissors => 3
  }

  @move_map %{
    "X" => @rock,
    "Y" => @paper,
    "Z" => @scissors
  }

  @goal_map %{
    "X" => :lose,
    "Y" => :tie,
    "Z" => :win
  }

  def part_1(games) do
    games
    |> Enum.map(&convert/1)
    |> Enum.map(&score/1)
    |> Enum.sum()
  end

  def part_2(games) do
    games
    |> Enum.map(&solve/1)
    |> Enum.map(&score/1)
    |> Enum.sum()
  end

  def answer do
    input = Helpers.load_list_file("priv/day_2_input.txt")

    games = Enum.map(input, &(String.split(&1, " ")))

    part_1 = part_1(games)

    IO.inspect(part_1)
      
    part_2 = part_2(games)
    IO.inspect(part_2)
  end

  def convert([theirs, mine]) do
    mine = Map.get(@move_map, mine)
    [theirs, mine]
  end

  def score([theirs, mine]) do
    outcome_pts = cond do
        theirs == mine ->
          3

        mine_wins?(theirs, mine) ->
          6

        true ->
          0
      end

    Map.get(@points, mine) + outcome_pts
  end

  def solve([theirs, goal]) do
    goal = Map.get(@goal_map, goal)

    my_move = case goal do
      :tie ->
        theirs

      :lose ->
        case theirs do
          @paper -> @rock
          @scissors-> @paper
          @rock-> @scissors
        end

      :win ->
        case theirs do
          @paper -> @scissors
          @scissors-> @rock
          @rock-> @paper
        end
    end

    [theirs, my_move]
  end

  def mine_wins?(theirs, mine) do
    case {mine, theirs} do
      {@paper, @rock} -> true
      {@rock, @scissors} -> true
      {@scissors, @paper} -> true
      _ -> false
    end
  end
end
